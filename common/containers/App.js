import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Counter from '../components/Counter'

import * as CounterActions from '../actions'

const mapStateToProps = (state) => ({
    counter: state.counter
})

function mapDispatchToProps(dispatch) {
    return bindActionCreators(CounterActions, dispatch)
}

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Counter)


export default AppContainer
