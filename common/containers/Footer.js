/**
 * Created by NBS on 2016-11-02.
 */

import React , {Component} from  'react';

// Create a Redux store holding the state of your app.
// Its API is { subscribe, dispatch, getState }.
let store = createStore(counter)

// You can use subscribe() to update the UI in response to state changes.
// Normally you'd use a view binding library (e.g. React Redux) rather than subscribe() directly.
// However it can also be handy to persist the current state in the localStorage.

store.subscribe(() =>
    console.log(store.getState())
)


class Footer extends Component{
    render(){
        const { status } = this.props
        console.log(status);
        return(
            <div>1234</div>
        )
    }

}

export default Footer;